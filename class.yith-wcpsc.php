<?php
/**
 * Main class
 *
 * @author Yithemes
 * @package YITH Product Size Charts for WooCommerce
 * @version 1.0.0
 */


if ( ! defined( 'YITH_WCPSC' ) ) {
    exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCPSC' ) ) {
    /**
     * YITH Product Size Charts for WooCommerce
     *
     * @since 1.0.0
     */
    class YITH_WCPSC {

        /**
         * Single instance of the class
         *
         * @var \YITH_WCPSC
         * @since 1.0.0
         */
        protected static $instance;

        /**
         * Plugin version
         *
         * @var string
         * @since 1.0.0
         */
        public $version = YITH_WCPSC_VERSION;

        /**
         * Plugin object
         *
         * @var string
         * @since 1.0.0
         */
        public $obj = null;

        /**
         * Returns single instance of the class
         *
         * @return \YITH_WCPSC
         * @since 1.0.0
         */
        public static function get_instance() {
            $self = __CLASS__ . ( class_exists( __CLASS__ . '_Premium' ) ? '_Premium' : '' );

            if ( is_null( $self::$instance ) ) {
                $self::$instance = new $self;
            }

            return $self::$instance;
        }

        /**
         * Constructor
         *
         * @return mixed| YITH_WCPSC_Admin | YITH_WCPSC_Frontend
         * @since 1.0.0
         */
        public function __construct() {

            // Load Plugin Framework
            add_action( 'after_setup_theme', array( $this, 'plugin_fw_loader' ), 1 );

            // Class admin
            if ( is_admin() ) {
                YITH_WCPSC_Admin();
            }else{
                YITH_WCPSC_Frontend();
            }
        }


        /**
         * Load Plugin Framework
         *
         * @since  1.0
         * @access public
         * @return void
         * @author Andrea Grillo <andrea.grillo@yithemes.com>
         */
        public function plugin_fw_loader() {

            if ( ! defined( 'YIT' ) || ! defined( 'YIT_CORE_PLUGIN' ) ) {
                require_once( 'plugin-fw/yit-plugin.php' );
            }

        }
    }
}

/**
 * Unique access to instance of YITH_WCPSC class
 *
 * @return \YITH_WCPSC
 * @since 1.0.0
 */
function YITH_WCPSC(){
    return YITH_WCPSC::get_instance();
}
?>